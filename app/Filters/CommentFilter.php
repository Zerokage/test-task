<?php

namespace App\Filters;

use App\Filters\AbstractFilter;

class CommentFilter extends AbstractFilter
{
    protected $filters = [
        'email' => Email::class,
        'name' => Username::class,
    ];
}
