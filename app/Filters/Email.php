<?php

namespace App\Filters;

class Email
{
    public function filter($builder, $value)
    {
        return $builder->where('email', $value);
    }
}
