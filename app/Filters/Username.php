<?php

namespace App\Filters;

class Username
{
    public function filter($builder, $value)
    {
        return $builder->where('name', $value);
    }
}
