<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Jobs\ProcessFileSaving;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderField = 'created_at';
        $orderDirection = 'desc';
        $items = Comment::where('parent_id', null)->filter(request())->with('file', 'replies')->orderBy($orderField, $orderDirection)->paginate(1);
        return response()->json($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCommentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCommentRequest $request)
    {
        $comment = new Comment();
        $comment->name = $request->name;
        $comment->email = $request->email;
        $comment->text = $request->text;
        $comment->link = $request->link;
        $comment->captcha = $request->captcha;
        $comment->parent_id = $request->parent_id;
        $comment->save();
        dispatch(new ProcessFileSaving($comment));
        return response()->json($comment);
    }

    public function captcha()
    {
        ob_start();
        include(app_path("\Library\captcha\captcha.php"));
        $contents =  ob_get_contents();
        ob_end_clean();
        $img_str = base64_encode($contents);
        $img_html = 'data:image/jpg;base64,'.$img_str;
        return response()->json($img_html);
    }
    
}
