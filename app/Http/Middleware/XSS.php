<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class XSS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $input = $request->all();
        array_walk_recursive($input,function(&$input) {
            $input = strip_tags($input);
        });
        $request->merge($input);
        return $next($request);
    }

    function xssAndSql($req)
    {
        $jsxss="onabort,oncanplay,oncanplaythrough,ondurationchange,onemptied,onended,onerror,onloadeddata,onloadedmetadata,onloadstart,onpause,onplay,onplaying,onprogress,onratechange,onseeked,onseeking,onstalled,onsuspend,ontimeupdate,onvolumechange,onwaiting,oncopy,oncut,onpaste,ondrag,ondragend,ondragenter,ondragleave,ondragover,ondragstart,ondrop,onblur,onfocus,onfocusin,onfocusout,onchange,oninput,oninvalid,onreset,onsearch,onselect,onsubmit,onabort,onbeforeunload,onerror,onhashchange,onload,onpageshow,onpagehide,onresize,onscroll,onunload,onkeydown,onkeypress,onkeyup,altKey,ctrlKey,shiftKey,metaKey,key,keyCode,which,charCode,location,onclick,ondblclick,oncontextmenu,onmouseover,onmouseenter,onmouseout,onmouseleave,onmouseup,onmousemove,onwheel,altKey,ctrlKey,shiftKey,metaKey,button,buttons,which,clientX,clientY,detail,relatedTarget,screenX,screenY,deltaX,deltaY,deltaZ,deltaMode,animationstart,animationend,animationiteration,animationName,elapsedTime,propertyName,elapsedTime,transitionend,onerror,onmessage,onopen,ononline,onoffline,onstorage,onshow,ontoggle,onpopstate,ontouchstart,ontouchmove,ontouchend,ontouchcancel,persisted,javascript";
        $jsxss = explode(",",$jsxss);
        foreach($req as $k=>$v)
        {
            if(is_array($v)) {
                foreach($v as $Kk=>$Vv) {
                    $Vv = preg_replace ( "'<script[^>]*?>.*?</script>'si", "", $Vv );
                    $Vv = str_replace($jsxss,"",$Vv);
                    $Vv = str_replace (array("*","\\"), "", $Vv );
                    $Vv = strip_tags($Vv);
                    $Vv = htmlentities($Vv, ENT_QUOTES, "UTF-8");
                    $Vv = htmlspecialchars($Vv, ENT_QUOTES);
                    $req[$k][$Kk] = $Vv;
                }
            } else {
                //Сначала удаляем любые скрипты для защиты от xss-атак
                $v = preg_replace ( "'<script[^>]*?>.*?</script>'si", "", $v );
                //Вырезаем все известные javascript события для защиты от xss-атак
                $v = str_replace($jsxss,"",$v);
                //Удаляем экранирование для защиты от SQL-инъекций
                $v = str_replace (array("*","\\"), "", $v );
                //Экранируем специальные символы в строках для использования в выражениях SQL
                //$v = mysql_real_escape_string( $v );
                //Удаляем другие лишние теги.	
                $v = strip_tags($v);
                //Преобразуем все возможные символы в соответствующие HTML-сущности
                $v = htmlentities($v, ENT_QUOTES, "UTF-8");
                $v = htmlspecialchars($v, ENT_QUOTES);
                //Перезаписываем GET массив
                $req[$k] = $v;
            }  
        }
        return $req;
    }
}
