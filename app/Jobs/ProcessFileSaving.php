<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Comment;
use App\Models\File;

class ProcessFileSaving implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $comment;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $req = request();
        
        $fileModel = new File();
        if($req->file('attachment')) {
            $attachment = $req->file('attachment');
            $fileName = time().'_'.$attachment->getClientOriginalName();
            $ext = $attachment->extension();
            if ($ext == 'txt') {
                $filePath = '/docs';
            } else {
                $filePath = '/images';
            }
            $filePath = $attachment->storeAs('uploads'.$filePath, $fileName, 'public');
            $fileModel->name = $fileName;
            $fileModel->file_path = '/storage/' . $filePath;
            $fileModel->ext = $ext;
            $this->comment->file()->save($fileModel);
        }
    }
}
