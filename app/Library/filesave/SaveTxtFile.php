<?php

namespace App\Library\FileSave;

use App\Models\File;

class SaveTxtFile
{
    protected $fieldName;
    protected $req;
    protected $parent;

    public function __construct($fieldName, $req, $parent)
    {
        $this->fieldName = $fieldName;
        $this->req = $req;
        $this->parent = $parent;
    }

    public function save()
    {
        $fileModel = new File();
        if($this->req->file()) {
            $fileName = time().'_'.$this->req->file->getClientOriginalName();
            $filePath = $this->req->file($this->fieldName)->storeAs('uploads', $fileName, 'public');
            $fileModel->name = time().'_'.$this->req->file->getClientOriginalName();
            $fileModel->file_path = '/storage/' . $filePath;
            $fileModel->save();
        }
    }
}
?>
