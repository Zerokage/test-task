<?php

namespace App\Models;

use App\Filters\CommentFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class Comment extends Model
{
    use HasFactory;

    public function file()
    {
        return $this->hasOne(File::class);
    }
    
    public function replies()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }
    
    
    public function parent()
    {
        return $this->belongsTo(self::class);
    }

    public function scopeFilter(Builder $builder, $request)
    {
        return (new CommentFilter($request))->filter($builder);
    }
}
