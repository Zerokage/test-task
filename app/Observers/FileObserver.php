<?php

namespace App\Observers;
use App\Models\File;
use Image;

class FileObserver
{
    public function saved(File $file)
    {
        $width =320; // your max width
        $height = 240; // your max height
        if ($file->ext != 'txt') {
            $storage_path = storage_path("/app/public/uploads/images/");
            $img = Image::make($storage_path.$file->name);
            if ($img->width() > $width || $img->height() > $height) {
                $img->resize($width, $height, function ($const) {
                    $const->aspectRatio();
                })->save($storage_path.'/thumbnails/'.$file->name);
            } else {
                $img->save($storage_path.'/thumbnails/'.$file->name);
            }
        }
    }
}
