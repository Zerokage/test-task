<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Captcha implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        session_start();
        $captcha = $_SESSION['captcha'];
        unset($_SESSION['captcha']);
        session_write_close();
        return $captcha == crypt(trim($value), '$1$itchief$7');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        #return 'Введенный код не соответствует изображению.';
        return 'Invalid captcha.';
    }
}
