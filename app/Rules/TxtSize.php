<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TxtSize implements Rule
{
    protected $request;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (is_object($value)) {
            $ext = $value->getClientOriginalExtension();
            if($ext == 'txt') {
                return $value->getSize()/1024 <= 100;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Txt file doesn\'t be bigger 100kb.';
    }
}
