//import CommentFormComponents from "./Components/CommentFormComponent.vue";
import AddForm from "./Components/AddForm.vue";
require("./bootstrap");

import { createApp } from "vue";
import { createStore } from "vuex";

const store = createStore({
    state() {
        return {
            email: "",
            name: "",
        };
    },
    getters: {
        NAME: (state) => {
            return state.name;
        },
        EMAIL: (state) => {
            return state.email;
        },
    },
    mutations: {
        SET_NAME: (state, payload) => {
            state.name = payload;
        },
        SET_EMAIL: (state, payload) => {
            state.email = payload;
        },
    },
});
const app = createApp({});

//app.component("comment-form-component", CommentFormComponents);
app.component("add-form", AddForm);

app.use(store).mount("#app");
