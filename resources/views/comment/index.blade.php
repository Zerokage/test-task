@extends('layouts.app')
@section('content')
    <h1>{{ __('Comments') }}</h1>
    <comment-form-component />
@endsection
