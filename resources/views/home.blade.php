@extends('layouts.app')

@section('content')
    <h1>Home</h1>
    <users-component v-bind:users="{{ $users->toJson() }}"></users-component>
@endsection
