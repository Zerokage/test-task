<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CommentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('comment.index');
});*/
Route::get('/', function () {
    return view('comment.index2');
});
Route::group(['middleware' => ['XSS']],function(){
    Route::controller(CommentController::class)->group(function () {
        Route::get('/comments/index', 'index');
        Route::post('/comments/index', 'index');
        Route::post('/comments', 'store')->name('comments');
        Route::get('/captcha', 'captcha')->name('captcha');
    });
});
