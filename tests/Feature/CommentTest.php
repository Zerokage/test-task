<?php

namespace Tests\Feature;

use App\Models\Comment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/comments/index');

        $response->assertStatus(200);

        $response = $this->post('/comments/index');

        $response->assertStatus(200);
    }
    public function test_can_list_posts() {
        $comments = factory(Comment::class, 2)->create()->map(function ($comments) {
            return $comments->only(['id', 'title', 'content']);
        });

        $this->get(route('/comments/index'))
            ->assertStatus(200)
            ->assertJson($comments->toArray())
            ->assertJsonStructure([
                '*' => [ 'id', 'title', 'content' ],
            ]);
    }
}
